const express = require('express')
const mongoose = require('mongoose');
const Movie = require('./Models/Movie')
const app = express()
const cors = require('cors')
const port = 3000

app.use(cors())
app.use(express.json())

app.get('/movies', async(req, res) => {
  const movies= await Movie.find({});
  res.status(200).json(movies)
})
app.post('/movies', async(req,res)=>{
   const movie=new Movie(req.body)
   await movie.save()
   res.status(201).json(movie)
} )

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('');
  console.log('connected')

}

